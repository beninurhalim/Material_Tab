package com.beninurhalim.a143040136.materialtab.fragment;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;
import com.beninurhalim.a143040136.materialtab.R;
public class Sub2Activity extends AppCompatActivity {

    public static String KEY_DATA = "data";
    private String receiveData = null;
    private TextView txtData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sub2);

        txtData = (TextView)findViewById(R.id.txt_data);
        receiveData = getIntent().getStringExtra(KEY_DATA);
        txtData.setText(receiveData);

    }
}
